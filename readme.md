

# Kerno 后端仓库

# 迁移至 Wiki，请在 [Wiki](https://github.com/SWPU-Kerno/backend-learning/wiki) 中查看

## 推荐学习路线与资源
### 阶段一 JavaSe 奠定基础

**基础语法（常用数据类型与包装类，访问与非访问修饰符，跳出循环，类与方法的声明）**

- 基本语法规则（标识符，修饰符，关键字）
- 数据类型与变量
- 修饰符与运算符
- 流程控制
- 数组
- 类与方法
- 泛型
- 常量与枚举
- 文档注释　


**面向对象（面向对象的编程描述，封装继承多态的联系，抽象类与接口的作用，内部类的实例化）**

- 面向对象概念（模型）
- 命名空间与访问级别
- 面向对象特性（封装，继承，多态，抽象）
- 面向接口编程（解耦和）
- 内部类（匿名，方法，成员，静态）
- Java面向对象语法规则总结

**工具类（字符串与可变字符串，日期类型转换与格式化，自定义异常，输入流与输出流的实现，线程控制，并发与锁）**

- 数据类型操作
- 字符串操作
- 数学运算
- 时间日期处理
- 异常处理
- 文件处理
- 流处理
- 多线程操作

**集合框架（数据结构的Java语言描述，集合框架的底层实现原理）**

- Java数据结构
- 集合接口
- 集合算法

**序列化（对象的序列化和反序列化操作）**

- 序列化
- 反序列化

**反射机制（类的反射实现，类中成员的反射实现）**

- 类反射
- 字段反射
- 方法反射
- 构造函数反射
- 反射对象创建
- 反射字段访问
- 数组反射

**注解（自定义注解的实现）**

- 常用注解与分类
- 自定义注解
- 注解解析

**文件处理（XML解析的常用方法，JSON的数据类型）**

- XML文件处理
- Properties文件处理
- JSON文件处理

**设计模式（工厂模式，单例模式，代理模式，观察者模式，MVC模式）**

- 创建型设计模式
- 结构型设计模式
- 行为型设计模式

**Java 基础部分推荐看下面这个视频，需要学习前 690 集的内容，后面的 关于JDK版本更新说明的内容 可以不用看：**

[Java 基础到高级-宋红康-零基础自学Java-尚硅谷](https://www.bilibili.com/video/BV1Qb411g7cz)

### 阶段二 MySQL数据库与项目实战

#### MySQL 学习

- [MySQL基础+MySQL高级+MySQL优化+MySQL34道作业题 ](https://www.bilibili.com/video/BV1fx411X7BD)

* MySQL必知必会 链接：https://pan.baidu.com/s/1IWeqx7IQGc0sY5bJ9WtyMA) 提取码:f1v9

学习 MySQL 的时候切忌只看视频，一定要跟着敲 SQL 命令。上面推荐的视频的每一道课后题都要跟着敲。

#### JDBC 学习

跟着教学视频敲一遍就行了，你不一定需要能直接写出代码，你需要理解整个JDBC的流程，以及其中的概念。

#### Maven 学习

这一部分挺重要的，需要认真学。

#### Git 学习

先学会 git clone/pull/commit/add 这几个最基础的命令，更高级的命令可以暂时不用学，之后项目开发需要用到的时候再学。

#### MyBatis 学习

需要认真学。

#### 使用已经学习到的知识写一个项目

推荐B站上找个视频 #todo 补充推荐视频。

### 阶段三 JavaWeb搭建网站

### 阶段四 SSM 到 Spring Boot

### 阶段五 准备面试 巩固基础

#### 学习方法

在这个阶段的重点就不是学习新的技术了，而是对已经学的东西进行巩固，针对面试要求进行学习。这个阶段是整个后端学习阶段中最为主要的阶段。

这个阶段涉及到：算法、JVM、Java 并发\MySQL进阶、计算机网络、操作系统、计算机组成原理。学有余力的可以学习 Redis 和 Netty。

#todo 学习方法、注意事项

#### 资料推荐

**所有书籍仅供学习使用**

---

**算法**

- 《算法（第4版）》本书翻译自 《Algorithms, 4th Edition》配合其官网使用
- alg4官网：https://algs4.cs.princeton.edu/home/ （英文网站，硬啃）
  - 《Alg4》书本源码：https://github.com/jimmysuncpt/Algorithms
- 《剑指Offer》：快餐式书籍，面试前的准备
- 在线刷题OJ：[LeetCode](https://leetcode-cn.com/) | [牛客](https://www.nowcoder.com/) 
- 📚书籍：https://pan.baidu.com/s/1LmUQI5IeFsGz0xV86x0wEw 提取码：upzl

---

**操作系统**

- 30天填自制操作系统  
   链接：https://pan.baidu.com/s/1QSIUlat1HpllNFMWY9WBzw 提取码:pxxr

- 操作系统之哲学原理  
  链接：https://pan.baidu.com/s/1GEfALJhtJ2IJtjtKcLneKg 提取码:hua7

- 程序是怎样跑起来的  
  链接：https://pan.baidu.com/s/1i_xDxT8QgA_x4kRHXEmPqg 提取码:pbvh

- **深入理解计算机操作系统**  
  链接：https://pan.baidu.com/s/1xrDC6wGAH7Yv3uuXzVUWxg 提取码:2toh

- 现代操作系统  
  链接：https://pan.baidu.com/s/1QA0VZ20pA8giW6ax7AHQBA 提取码:huk2

---

 **计算机网络**

- **网络是怎样连接的**  
  链接：https://pan.baidu.com/s/1HhE9MoC4rp5NSvyWOtk_hw 提取码:g983
- **图解HTTP**  
  链接：https://pan.baidu.com/s/1NEAK2PH_4JzPY454QvmmsQ 提取码:b42z
- **图解TCP/IP**  
  链接：https://pan.baidu.com/s/1FJru4ppyxBhx_J3pk8rpvA 提取码:5k3x
- **计算机网络：自顶向下**  
  链接：https://pan.baidu.com/s/18X_L2RINmtLK9g0Ki2GiFw 提取码:l77d
- HTTP权威指南  
  链接：https://pan.baidu.com/s/1b2fIsmdt9ANVMcqU97t6kw 提取码:tqj8
- UNIX网络编程  
  链接：https://pan.baidu.com/s/1vXbj7OLsouVFH4DCIMqF3w 提取码:4buy

---

**计算机组成原理**

- 隐匿在计算机软硬件背后的语言  
  链接：https://pan.baidu.com/s/1gJCnChBTCZD07mm3IqCRwQ 提取码:6jiq
- 大话计算机  
  链接：https://pan.baidu.com/s/1AakpZmD8hziokFnV0VSGFA 提取码:6j1o
- 计算机是怎样跑起来的  
  链接：https://pan.baidu.com/s/1JIq3U8kNYohz0C3xnCk6EA 提取码:er12

---

**JVM**

- [尚硅谷JVM视频](https://www.bilibili.com/video/BV1PJ411n7xZ?from=search&seid=7262462400362845072)
  
- **深入理解java虚拟机** 
  链接：https://pan.baidu.com/s/1H8G4e1b-T089VW9bnuQ7xw 提取码：168h 

- [尚硅谷jvm笔记](https://www.yuque.com/u21195183/jvm/qpoa81)

---

**Java并发**

- [黑马程序员全面深入学习java并发编程](https://www.bilibili.com/video/BV16J411h7Rd)

- **并发编程的艺术**  
  链接：https://pan.baidu.com/s/1TxZjweACgcScAnf2B_sB9w   提取码：akqo 

- **Java并发编程之美**   
  链接：https://pan.baidu.com/s/12FfKQX4QkSwi8Uq6TmokRQ   提取码：aqev 

- **Java并发编程实战**   
  链接：https://pan.baidu.com/s/19XqMBrm-8rScukM-C8hUbA   提取码：symw

---

**Mysql数据库**

- **高性能MySQL**  
  链接：https://pan.baidu.com/s/1_vXaan9KbIWAHEnyQnQFlg 提取码:gh5t

- 深入浅出MySQL  
  链接：https://pan.baidu.com/s/1gWJcwszv3CnjIRMzy--_rw 提取码:ju0h

- Mysql是怎样运行的   

  链接：https://pan.baidu.com/s/18zNUWpesg-t7KO6u0BRBZQ 提取码：br2u 
  
- **MySQL技术内幕InnoDB存储引擎**  
  链接：https://pan.baidu.com/s/1fLL3GXn_Hp6DdEnHXcBFJQ 提取码:6g04

---

**Redis**

- **Redis设计与实现**  
  链接：https://pan.baidu.com/s/1b8lq_b2POU-QSyvtsdaxdg   提取码：l1yp 

- **Redis开发与运维**  
  链接：https://pan.baidu.com/s/1gxEf1dZNbdjtkN7k5NEJUQ   提取码：5y8n 

- **Redis深度历险** 
  链接：https://pan.baidu.com/s/1rAHSdaEkKAMp9l2WsieOeQ   提取码：hl09 

---

**Netty**

- [尚硅谷netty视频](https://www.bilibili.com/video/BV1DJ411m7NR?from=search&seid=2608629258839127601)
- [尚硅谷netty视频笔记](https://dongzl.github.io/netty-handbook/#/README)

*  Netty权威指南
  链接：https://pan.baidu.com/s/1sVZFqMjKjsonvV_pMAgXyg   提取码：3p2i

* Netty实战
  链接：https://pan.baidu.com/s/1zvjvFEB4SCwvOZIVov_EKA   提取码：8o5p

---

**设计模式**

- [黑马设计模式](https://www.bilibili.com/video/BV1Np4y1z7BU) （不推荐看视频，建议直接看书，目前没有好的视频2021/6/23）

- 「**菜鸟教程|设计模式**」：https://www.runoob.com/design-pattern/design-pattern-tutorial.html
- 「**Refactoring.Guru**」：https://refactoringguru.cn/design-patterns
- 📚书籍：《设计模式之禅 第二版》《重学Java设计模式》
  - 链接：https://pan.baidu.com/s/1E1DJ3VVOm6EvM4wR0QoSAw 提取码：asg4

---

**Git**

- [尚硅谷Git](https://www.bilibili.com/video/BV1vy4y1s7k6) 
- [菜鸟教程|图解Git](https://www.runoob.com/w3cnote/git-graphical.html)
- [高质量的Git中文教程](https://github.com/geeeeeeeeek/git-recipes)

### 阶段六 Java分布式与微服务

### 阶段七

## 推荐学习仓库

- [shopping-management-system](https://github.com/zhanglei-workspace/shopping-management-system)


## 推荐博客/专栏/微信公众号

- [美团技术团队](https://tech.meituan.com/)
- [InfoQ](https://www.infoq.cn/)
- [小林Coding](https://www.cnblogs.com/xiaolincoding)
- [bugstack虫洞栈](https://bugstack.cn/)
- [徐靖峰|个人博客](https://lexburner.github.io/)
- [廖雪峰官方博客](https://www.liaoxuefeng.com/)

推荐使用 Inoreader(Chrome插件) 订阅这些博客，获取更新推送。

- 微信公众号 - 阿里技术
- 微信公众号 - 腾讯技术工程
- 微信公众号 - 三太子敖丙
- 微信公众号 - 程序员cxuan
- 微信公众号 - 阿里巴巴云原生

## More

**不懂或者有疑惑的话直接问学长学姐, 别迷茫**

## 仓库维护日志

**如果你想在这个仓库中添加更多的内容或者优化仓库内容，请 fork 本仓库，并提交 pull request 。**

2021/7/11 添加部分细节

2021/6/23 添加Git、设计模式、算法推荐资料

2020/6/21 完善整体路线、添加推荐书籍

2021/6/20 新建仓库
